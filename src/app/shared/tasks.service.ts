import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as moment from 'moment';

export interface Task {
  id?: string;
  title: string;
  date?: string;
}

interface CreateResponse {
  name: string;
}

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  static publicURL = 'https://angular-practice-calenda-be423.firebaseio.com/';

  constructor(public http: HttpClient) {}

  load(date: moment.Moment): Observable<Task[]> {
    return this.http
      .get<Task[]>(
        `${TasksService.publicURL}/${date.format('YYYY-MM-DD')}.json`
      )
      .pipe(
        map((res) => {
          if (!res) {
            return [];
          }
          return Object.keys(res).map((key) => ({ ...res[key], id: key }));
        })
      );
  }

  create(task: Task): Observable<Task> {
    return this.http
      .post<CreateResponse>(`${TasksService.publicURL}/${task.date}.json`, task)
      .pipe(
        map((res) => {
          return { ...task, id: res.name };
        })
      );
  }

  remove(task: Task) {
    return this.http.delete(
      `${TasksService.publicURL}/${task.date}/${task.id}.json`
    );
  }
}
