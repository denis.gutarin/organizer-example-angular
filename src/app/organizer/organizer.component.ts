import { Component, OnInit } from '@angular/core';
import { DateService } from '../shared/date.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Task, TasksService } from '../shared/tasks.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss'],
})
export class OrganizerComponent implements OnInit {
  form: FormGroup;
  tasks: Task[] = [];

  constructor(
    public dateService: DateService,
    public tasksService: TasksService
  ) {}

  ngOnInit(): void {
    this.dateService.date
      .pipe(switchMap((value) => this.tasksService.load(value)))
      .subscribe(
        (tasks) => (this.tasks = tasks),
        (error) => console.log(error.text)
      );
    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
    });
  }

  remove(task: Task) {
    this.tasksService.remove(task).subscribe(
      () => (this.tasks = this.tasks.filter((item) => item.id !== task.id)),
      (error) => {
        console.log(error.text);
      }
    );
  }

  submit() {
    const { title } = this.form.value;
    const task: Task = {
      title,
      date: this.dateService.date.value.format('YYYY-MM-DD'),
    };

    this.tasksService.create(task).subscribe(
      (value: Task) => {
        this.form.reset();
        this.tasks.push(value);
      },
      (error) => {
        console.log(error.text);
      }
    );
  }
}
